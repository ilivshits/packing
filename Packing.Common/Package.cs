﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Packing.Common
{
    public class Package
    {
        public Package(int width)
        {
            Width = width;
        }

        public IList<Level> Levels { get; set; } = new List<Level>();

        public int Width { get; set; }

        public int Height
        {
            get { return Levels.Sum(level => level.Height); }
        }
    }
}
