﻿using System.Collections.Generic;
using System.Linq;

namespace Packing.Common
{
    public class Level
    {
        public IList<Rectangle> FloorObjects { get; set; } = new List<Rectangle>();
        public IList<Rectangle> CeilingObjects { get; set; } = new List<Rectangle>();

        public int Height
        {
            get { return FloorObjects.Any() ? FloorObjects.Max(r => r.Height) : 0; }
        }

        public int FloorWidth
        {
            get
            {
                return FloorObjects.Any() ? FloorObjects.Sum(r => r.Width) : 0;
            }
        }

        public int CeilingWidth
        {
            get
            {
                return CeilingObjects.Any() ? CeilingObjects.Sum(r => r.Width) : 0;
            }
        }
    }
}
