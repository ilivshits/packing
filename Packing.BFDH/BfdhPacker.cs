﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;
using Packing.Common;

namespace Packing.BFDH
{
    public class BfdhPacker : IPacker
    {
        public void Pack(Package package, IEnumerable<Rectangle> objects)
        {
            var sortedObjects = objects.OrderByDescending(r => r.Height).ToList();

            foreach (var rect in sortedObjects)
            {
                AddToSuitableLevel(package, rect);
            }
        }

        private void AddToSuitableLevel(Package package, Rectangle rect)
        {
            List<Level> possibleLevels = package.Levels.Where(level => package.Width > level.FloorWidth + rect.Width).ToList();

            if (!possibleLevels.Any())
            {
                var newLevel = new Level();
                newLevel.FloorObjects.Add(rect);
                package.Levels.Add(newLevel);
                
                return;
            }

            possibleLevels.MinBy(level => package.Width - level.FloorWidth - rect.Width).FloorObjects.Add(rect);
        }
    }
}
